Source: django-ratelimit
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-django,
               python3-setuptools,
               python3-sphinx
Testsuite: autopkgtest-pkg-python
Standards-Version: 4.6.2
Homepage: https://github.com/jsocol/django-ratelimit
Vcs-Browser: https://gitlab.com/kalilinux/packages/django-ratelimit
Vcs-Git: https://gitlab.com/kalilinux/packages/django-ratelimit.git

Package: python3-django-ratelimit
Architecture: all
Depends: python3-django, ${misc:Depends}, ${python3:Depends}
Suggests: python-django-ratelimit-doc
Description: provides a decorator to rate-limit views (Python 3)
 This package contains Django Ratelimit which provides a decorator to
 rate-limit views. Limiting can be based on IP address or a field in the
 request--either a GET or POST variable.
 .
 This package installs the library for Python 3.

Package: python-django-ratelimit-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: provides a decorator to rate-limit views (common documentation)
 This package contains Django Ratelimit which provides a decorator to
 rate-limit views. Limiting can be based on IP address or a field in the
 request--either a GET or POST variable.
 .
 This is the common documentation package.
